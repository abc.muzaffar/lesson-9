const display = document.getElementById("result");
const result = document.getElementById("h1")
const buttons = Array.from(document.getElementsByClassName("btn"))

buttons.map(button => {
    button.addEventListener('click', (e) => {
        switch (e.target.innerText) {
            case 'C':
                display.innerText = '';
                break;
            case '=':
                try {
                    if(eval(display.innerText) == undefined) break;
                    display.innerText = eval(display.innerText);
                    result.innerText = eval(display.innerText);
                } catch (error) {
                    display.innerHTML = 'Error!';
                }
                break;
            case '%':
                if (display.innerText != '') {
                    display.innerText = eval(display.innerText) / 100;
                    result.innerText = eval(display.innerText);
                }
                break;
            default:
                if (display.innerText != 0 )
                    display.innerText += e.target.innerText;
                else display.innerText = e.target.innerText;
        }
    })
})
